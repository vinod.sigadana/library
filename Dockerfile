FROM openjdk:8-alpine
ADD build/libs/*.jar library.jar
ENTRYPOINT ["java", "-jar", "library.jar"]